package com.demo.springboot.services.implementation;

import com.demo.springboot.domain.dto.MovieDto;
import com.demo.springboot.services.MoviesService;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class MoviesServiceImpl implements MoviesService {
    @Override
    public List<MovieDto> loadMovies() {

        List<MovieDto> movies = new ArrayList<>();

        String csvFile = "src/main/resources/movies.csv";
        String line = "";
        String cvsSplitBy = ";";

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] moviesArray = line.split(cvsSplitBy);
                movies.add(new MovieDto(moviesArray[0].trim(), moviesArray[1].trim(), moviesArray[2], moviesArray[3]));

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return movies;
    }
}
