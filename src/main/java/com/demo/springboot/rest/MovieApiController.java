package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.MovieListDto;
import com.demo.springboot.services.MoviesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class MovieApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);

    private MoviesService moviesService;

    @Autowired
    public MovieApiController(MoviesService moviesService) {
        this.moviesService = moviesService;
    }

    @RequestMapping(value = "/movies")
    @ResponseBody
    @CrossOrigin
    public MovieListDto getMovies() {

        LOGGER.info("--- get movies");

        return new MovieListDto(moviesService.loadMovies());
    }
}